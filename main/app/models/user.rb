class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable, :timeoutable
		 
	
	
	# in User
ROLES = %w[gen_admin teacher_admin student]
def role?(base_role)
  ROLES.index(base_role.to_s) <= ROLES.index(role)
end
end
