require 'test_helper'

class Gti619ControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get circle" do
    get :circle
    assert_response :success
  end

  test "should get square" do
    get :square
    assert_response :success
  end

  test "should get admin_Panel" do
    get :admin_Panel
    assert_response :success
  end

end
